#install.packages("shiny")
#install.packages("ggvis")

rm(list = ls())
options(shiny.trace = F)  # cahnge to T for trace
require(shiny)
require(shinysky)
library(shinyjs)
source("scraping_metacretic.R")

#CreateAndUpdateCsv('anthony-toste')
#CreateAndUpdateCsv("john-boyega")
#CreateAndUpdateCsv("forest-whitaker")

getMoviesScrapur <- function() {
  filename <- 'comp.csv'
  if (file.exists(filename)) {
    ratings <- read.csv(filename)
    title <- unique(ratings$Title)
    return(ratings$title)
  }
  return()
}

setUp <- function(filename){
  if (file.exists('metacritic_data.csv')) {
    file.remove('metacritic_data.csv')
  }
  if (file.exists('comp.csv')) {
    file.remove('comp.csv')
  }
  
  CreateAndUpdateCsv("john-boyega")
  CreateAndUpdateCsv("forest-whitaker")
  CreateAndUpdateCsv("sam-worthington")
  CreateAndUpdateCsv("george-clooney")
  CreateAndUpdateCsv("johnny-depp")
  CreateAndUpdateCsv("tom-hanks")
  CreateAndUpdateCsv("christian-bale")
  CreateAndUpdateCsv("robert-de-niro")
  CreateAndUpdateCsv("leonardo-diCaprio")
  CreateAndUpdateCsv("morgan-freeman")
  CreateAndUpdateCsv("al-pacino")
  CreateAndUpdateCsv("gary-oldman")
  CreateAndUpdateCsv("matt-damon")
  
  CreateAndUpdateComp()
}

CreateAndUpdateComp <- function() {
  filename <- 'comp.csv'
  metacritic_data_set <- read.csv("metacritic_data.csv")
  imdb_data_set <- read.csv("./data/imdb/IMDB-Movie-Data.csv")
  actor_ratings <- merge(metacritic_data_set, imdb_data_set, by = "Title")
  if (file.exists(filename)) {
    actor_ratings <- rbind(read.csv(filename), actor_ratings)
  }  
  write.csv(actor_ratings, filename, row.names = FALSE)
}

shinyServer(function(input, output, session) {

  filename <- 'comp.csv'
  
  setUp(filename)
  
  if (file.exists(filename)) {
    imdb_data_set <- read.csv("./data/imdb/IMDB-Movie-Data.csv")
    ratings <- read.csv(filename)
    ratings <- ratings[complete.cases(ratings),]
    ratings$rel_year <- as.Date(ratings$rel_year, '%Y-%m-%d')
    ratings$year <- as.integer(format(ratings$rel_year, '%Y'))
    ratings$Title <- subset(ratings, select = c("Title"))
    ratings$Rating <- as.numeric(gsub(",", ".", ratings$Rating))*10
    
    
    ratings <- ratings[with(ratings, order(Year)),]
    ratings$mean_score <- rowMeans(subset(ratings, select = c("user_score", "Rating"), na.rm = TRUE))

    minYear <- min(ratings$Year)
    maxYear <- max(ratings$Year)
    #genres <- unique(unlist(lapply(ratings[ratings$genres != "","Genre"], function(x) strsplit(as.character(x), " "))))
    
    output$yearSlider <- renderUI(
      sliderInput(
        "yearSlider",
        "Release year:",
        minYear,
        maxYear,
        c(minYear,maxYear),
        step = 1,
        sep = ''
      )
    )
    
    output$yearSlider <- renderUI(
      sliderInput(
        "yearSlider",
        "Release year:",
        minYear,
        maxYear,
        c(minYear,maxYear),
        step = 1
      )
    )
    
    sliderValues2 <- reactive({
      data.frame(
        Name = c("Year"),
        Value = as.character(c( paste(input$yearSlider, collapse = " "))),
        stringsAsFactors = FALSE)
    })
    
    output$values2 <- renderTable({
      sliderValues2()
    })
    
    output$ratingSlider <- renderUI(
      sliderInput(
        "ratingSlider",
        "Rating:",
        0,
        100,
        c(0, 100),
        step = 1
      )
    )
    
    sliderValues <- reactive({
      data.frame(
        Name = c("Rating"),
        Value = as.character(c( paste(input$ratingSlider, collapse = " "))),
        stringsAsFactors = FALSE)
    })
    
    output$values <- renderTable({
      sliderValues()
    })
  
    #gener
    output$select2Input1 <- renderText({
      input$select2Input1
    })

    data <- reactive ({
      data_rating <- ratings[
        ratings$Rating > min(input$ratingSlider)
        & ratings$user_score > min(input$ratingSlider)
        & ratings$Rating < max(input$ratingSlider)
        & ratings$user_score < max(input$ratingSlider)
        ,
        ]
      
      data_rating <- data_rating[data_rating$year >= min(input$yearSlider) & data_rating$year <= max(input$yearSlider),]
      
#      data_rating <- data_rating[
#        grep(paste(input$select2Input1, collapse="|"), data_rating$genres),
#        ]
      
    #  if(input$thti != ""){
    #    data_rating <- ratings[
    #      ratings$Title == input$thti,
    #      ]  
    #    print(data_rating$Title)
    #  }

      data_rating$cum_imdb <- cumsum(data_rating$Rating)
      data_rating$cum_user <- cumsum(data_rating$user_score)
      
      data_rating
    })

    vis <- reactive({
      xvar_name <- "metacritic user score"
      yvar_name <- "imdb user score"
      
      data() %>%
        ggvis(~Rating, ~user_score) %>%
        layer_points(size := 50, size.hover := 200, fillOpacity := 0.5, fillOpacity.hover := 1) %>%
        add_axis("x", title = xvar_name) %>%
        add_axis("y", title = yvar_name) %>%
        scale_numeric("x", domain = c(0, 100)) %>%
        scale_numeric("y", domain = c(0, 100))
    })
    
    liner <- reactive({
      xvar_name <- "Release date"
      yvar_name <- "Score"
      
      data() %>%
        ggvis(~Year, y = ~cum_imdb) %>%
        layer_paths(stroke := "red") %>%
        layer_paths(data = data(), x = ~Year, y = ~cum_user, stroke := "green") %>%
        add_axis("x", title = xvar_name) %>%
        add_axis("y", title = yvar_name)
    })
    
    #on/of crappuurh button
    observeEvent(input$thti, {
      shinyjs::hide("id_double_click_event")
      show_button <- FALSE
      if(input$thti != ""){
        vis %>% bind_shiny("plot")
        shinyjs::hide("linePlot")  
        show_button <- input$thti %in% getMoviesScrapur()
        if(show_button == FALSE){
          print(show_button)
          shinyjs::show("id_double_click_event")
        }
        if(show_button == TRUE){
          print(show_button)
          shinyjs::hide("id_double_click_event")       
        }
      }
    })
    
    liner %>% bind_shiny("linePlot")
    
    # crappuurh Button
    observe({
      if (is.null(input$id_double_click_event)) {
        return()
      }
      if (input$id_double_click_event$event == "dblclick") {
        movie_row <- subset(imdb_data_set, Title == input$thti)
        actors_row <- movie_row$Actors
        first_actor <- strsplit(toString(actors_row), ",")[[1]]
        print(first_actor[1])
        CreateAndUpdateCsv(first_actor[1])
        CreateAndUpdateComp()
        shinyjs::hide("id_double_click_event")
      }
    })
  }  
})

